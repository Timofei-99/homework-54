import './App.css';
import {nanoid} from 'nanoid';
import GamePlace from "./Components/GamePlace/GamePlace";
import {useState} from "react";
import Square from "./Components/Square/Square";
import Counter from "./Components/Counter/Counter";
import Button from "./Components/Button/Button";



const App = () => {
    const [square, setSquare] = useState([]);
    const [count, setCount] = useState(0);

    const getRandomIntInclusive = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    if (square.length === 0) {
        const rndNumber = getRandomIntInclusive(0, 35);
        let newArray = [];
        for (let i = 0; i < 36; i++) {
            if (rndNumber === i) {
                newArray.push({hasItem: 'O', id: nanoid(), hidden: false});
            } else {
                newArray.push({hasItem: '', id: nanoid(), hidden: false});
            }
            setSquare(newArray);
        }
    }

    console.log(square)


    const onClick = id => {
        setSquare(square.map((p => {
            if (p.hidden === false) {
                if (p.id === id) {
                    setCount(count + 1);
                    return {...p, hidden: true}
                }
            }
            return p;
        })));
    };

    const btnClick = () => {
        setSquare([]);
        setCount(0);
    };




    const mySquares = square.map(el => (
       <Square element={el.hasItem}
               hidden={el.hidden}
               key={el.id}
               click={() => onClick(el.id)}
       />
   ));


  return (
    <div className="App">
      <GamePlace lines={mySquares}/>
        <Counter count={count}/>
        <Button reset={btnClick}/>
    </div>
  );
};

export default App;
