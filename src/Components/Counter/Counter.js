import React from 'react';

const Counter = props => {
    return (
        <div>
            <p>Tries: {props.count}</p>
        </div>
    );
};

export default Counter;