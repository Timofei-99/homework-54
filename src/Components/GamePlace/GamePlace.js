import React from 'react';
import './GamePlace.css';

const GamePlace = props => {
    return (
        <div className='mainSquare'>
            {props.lines}
        </div>
    );
};

export default GamePlace;