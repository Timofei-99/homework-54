import React from 'react';
import '../Button/Button.css'

const Button = props => {
    return (
        <button type='button' onClick={props.reset} className='myBtn'>
            Reset
        </button>
    );
};

export default Button;