import React from 'react';
import './Square.css';

const Square = props => {
    let classes = '';
    if (props.hidden) {
        classes = ['square white'];
    } else {
        classes = ['square black'];
    }



    return (
        <div className={classes} key={props.id} onClick={props.click} >
            <span className='oshka'>{props.element}</span>
        </div>
    );
};

export default Square;